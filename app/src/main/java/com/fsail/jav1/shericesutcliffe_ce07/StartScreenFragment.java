// Sherice Sutcliffe
// MDF III - 1707
// StartScreenFragment.java

package com.fsail.jav1.shericesutcliffe_ce07;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;


public class StartScreenFragment extends Fragment implements View.OnClickListener {

    public static StartScreenFragment newInstance() {

        Bundle args = new Bundle();

        StartScreenFragment fragment = new StartScreenFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View inflateView = inflater.inflate(R.layout.start_screen_fragment, container, false);

        Button start = (Button)inflateView.findViewById(R.id.startButton);
        Button credits = (Button)inflateView.findViewById(R.id.creditsButton);

        start.setOnClickListener(this);
        credits.setOnClickListener(this);

        return inflateView;
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.startButton) {
            // go to GameActivity
            Intent intent = new Intent(getContext(), GameActivity.class);
            getActivity().startActivity(intent);

        } else if (v.getId() == R.id.creditsButton) {
            // go to CreditsActivity
            Intent intent = new Intent(getContext(), CreditsActivity.class);
            getActivity().startActivity(intent);

        }
    }
}
