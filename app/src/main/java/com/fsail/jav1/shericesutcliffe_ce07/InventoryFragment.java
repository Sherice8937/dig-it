// Sherice Sutcliffe
// MDF III - 1707
// InventoryFragment.java

package com.fsail.jav1.shericesutcliffe_ce07;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;


public class InventoryFragment extends Fragment {

    private ArrayList<Item> allItems = new ArrayList<>();

    public static InventoryFragment newInstance() {

        Bundle args = new Bundle();

        InventoryFragment fragment = new InventoryFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View inflateView = inflater.inflate(R.layout.inventory_fragment, container, false);

        allItems = GameSurfaceView.foundItems;

        ListView listView = (ListView)inflateView.findViewById(R.id.listView);
        TextView title = (TextView) inflateView.findViewById(R.id.title);

        ArrayAdapter<Item> adapter = new ArrayAdapter<>
                (getContext(), android.R.layout.simple_list_item_1, allItems);
        listView.setAdapter(adapter);

        String total = "Total Score: " + String.valueOf(totalGoldCount());
        title.setText(total);

        return inflateView;
    }

    private int totalGoldCount() {
        int total = 0;
        for (Item i : allItems) {
            total += i.getGold();
        }

        return total;
    }
}
