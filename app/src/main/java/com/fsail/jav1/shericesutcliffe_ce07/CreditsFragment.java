// Sherice Sutcliffe
// MDF III - 1707
// CreditsFragment.java

package com.fsail.jav1.shericesutcliffe_ce07;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


public class CreditsFragment extends Fragment {

    public static CreditsFragment newInstance() {

        Bundle args = new Bundle();

        CreditsFragment fragment = new CreditsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.credits_fragment, container, false);
    }
}
