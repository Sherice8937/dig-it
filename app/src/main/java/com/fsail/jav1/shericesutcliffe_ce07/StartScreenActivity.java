// Sherice Sutcliffe
// MDF III - 1707
// StartScreenActivity.java

package com.fsail.jav1.shericesutcliffe_ce07;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class StartScreenActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.start_screen);

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.startFrameLayout, StartScreenFragment.newInstance()).commit();
        }
    }
}
