// Sherice Sutcliffe
// MDF III - 1707
// GameSurfaceView.java

package com.fsail.jav1.shericesutcliffe_ce07;


import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Random;

public class GameSurfaceView extends SurfaceView implements SurfaceHolder.Callback {

    private static final String TAG = "GameSurfaceView";
    private int total_items;

    private Rect mDimensions;
    private Bitmap mBackground;

    private Paint mBlankPaint;
    private Paint mTextPaint;
    private Bitmap mDirtHolePaint;

    private ArrayList<Point> mPoints;

    // arrayList to hold items read in from CSV file
    private ArrayList<Item> allItems;
    // arrayList to hold found items
    static final ArrayList<Item> foundItems = new ArrayList<>();

    public GameSurfaceView(Context context) {
        super(context);
    }

    public GameSurfaceView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public GameSurfaceView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public GameSurfaceView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        foundItems.clear();

        setWillNotDraw(false);
        getHolder().addCallback(this);

        Resources res = getResources();
        mBackground = BitmapFactory.decodeResource(res, R.drawable.field);

        mBlankPaint = new Paint();

        mTextPaint = new Paint();
        mTextPaint.setColor(Color.WHITE);
        mTextPaint.setTextSize(60.0f);

        mDirtHolePaint = BitmapFactory.decodeResource(res, R.drawable.hole);

        mPoints = new ArrayList<>();

    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        // clear the canvas by drawing a single color
        canvas.drawColor(Color.BLACK);

        // draw our background image to fill the entire dimensions of the canvas
        canvas.drawBitmap(mBackground, null, mDimensions, mBlankPaint);

        // draw a circle at each touch point
        for (Point p : mPoints) {
            canvas.drawBitmap(mDirtHolePaint, p.x, p.y, null);
        }

        // draw text at the top of the screen
        if (allItems != null) {
            canvas.drawText(total_items - foundItems.size() + " Pieces of Treasure Remaining", mDimensions.width() / 10.0f,
                    mDimensions.height() / 10.0f, mTextPaint);
        } else {
            canvas.drawText("170 Pieces of Treasure Remaining", mDimensions.width() / 10.0f,
                    mDimensions.height() / 10.0f, mTextPaint);
        }

        if (allItems == null) {
            readFile();
            Log.i(TAG, "onDraw: READING FILES...");
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        // check if this touch event is a down event
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            // add the point to our list
            mPoints.add(new Point(
                    (int)event.getX(),
                    (int)event.getY()
            ));

            findItems(event);

            postInvalidate();
        }


        return super.onTouchEvent(event);
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        storeDimensions(holder);
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        storeDimensions(holder);
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {

    }

    private void storeDimensions(SurfaceHolder holder) {
        // lock the canvas to get an instance of it back
        Canvas canvas = holder.lockCanvas();

        // retrieve the dimensions and hold onto them for later
        mDimensions = new Rect(0, 0, canvas.getWidth(), canvas.getHeight());

        // release the canvas and pose a draw
        holder.unlockCanvasAndPost(canvas);
    }

    private void readFile() {
        allItems = new ArrayList<>();

        // get a random float for x
        Random randX = new Random();

        // get a random float for y
        Random randY = new Random();

        InputStream inputStream = getResources().openRawResource(R.raw.items);

        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));

        try {
            String csvLine;
            while ((csvLine = reader.readLine()) != null) {
                String[] row = csvLine.split(",");
                int posX = randX.nextInt(mDimensions.width());
                int posY = randY.nextInt(mDimensions.height());
                allItems.add(new Item(row[0], Integer.parseInt(row[1]), posX, posY));
                inputStream.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        total_items = allItems.size();
    }

    private void findItems(MotionEvent event) {
        // get the X and Y of the location the user clicked
        int eventX = (int)event.getX();
        int eventY = (int)event.getY();

        // check if eventX and eventY are within a radius of each other
        for (Item i : allItems) {
            // check if item is in radius of X coordinate
            if (i.getPositionX() <= eventX + 20 && i.getPositionX() >= eventX - 20) {
                // check if item is in radius of Y coordinate
                if (i.getPositionY() <= eventY + 20 && i.getPositionY() >= eventY - 20) {
                    // tell the user which item was found
                    Toast.makeText(getContext(), "FOUND: " + i.getName(), Toast.LENGTH_SHORT).show();
                    // add item to the foundItems array
                    foundItems.add(i);
                    if (foundItems.contains(i)) {
                        // if the item is already found, break
                        break;
                    }
                }
            }
        }
    }

}
