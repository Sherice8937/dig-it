// Sherice Sutcliffe
// MDF III - 1707
// GameActivity.java

package com.fsail.jav1.shericesutcliffe_ce07;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class GameActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.game);

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.gameFrameLayout, GameFragment.newInstance()).commit();
        }
    }
}
