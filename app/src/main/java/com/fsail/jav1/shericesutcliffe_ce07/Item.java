// Sherice Sutcliffe
// MDF III - 1707
// Item.java

package com.fsail.jav1.shericesutcliffe_ce07;


import java.io.Serializable;

class Item implements Serializable {

    private final String name;
    private final int gold;
    private final int positionX;
    private final int positionY;

    public Item(String name, int gold, int positionX, int positionY) {
        this.name = name;
        this.gold = gold;
        this.positionX = positionX;
        this.positionY = positionY;
    }

    public String getName() {
        return name;
    }

    public int getGold() {
        return gold;
    }

    public int getPositionX() {
        return positionX;
    }

    public int getPositionY() {
        return positionY;
    }

    @Override
    public String toString() {
        return name;
    }
}
