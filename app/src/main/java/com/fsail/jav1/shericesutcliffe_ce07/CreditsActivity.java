// Sherice Sutcliffe
// MDF III - 1707
// CreditsActivity.java

package com.fsail.jav1.shericesutcliffe_ce07;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class CreditsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.credits);

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.creditsFrameLayout, CreditsFragment.newInstance()).commit();
        }
    }
}
