// Sherice Sutcliffe
// MDF III - 1707
// InventoryActivity.java

package com.fsail.jav1.shericesutcliffe_ce07;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class InventoryActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.inventory);

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.inventoryFrameLayout, InventoryFragment.newInstance()).commit();
        }
    }
}
