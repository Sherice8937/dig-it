# README #

Dig It!
by Sherice Sutcliffe

### About ###

* Dig It! was created in approx. 1 day
* It is a simple treasure-finding app, where you click around and "dig" for treasure

### Set Up Requirements ###

* 21+ API

### Contact ###

* Repo owner/admin: Sherice Sutcliffe
* Email: sherice.sutcliffe8937@yahoo.com

## Please contact me if you wish to download/use this project in any way